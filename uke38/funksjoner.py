# Det er forskjell på 
# - Hva skal vi gjøre
# og
# - Hvordan vi gjør det
# Man kan programmere vanskelig, eller smart.
# Man kan bruke ting flere ganger, til og med andres ting!

# Eksempel 1: Finne lengden av en streng
#man kan skrive dette:

streng = 'detteernåenutruuuuliglangstrengsomi' \
         'allefallikkejeggidderåtelletegnenetilfordahadde' \
         'jegsikkertholdtpåtilimorgen!'

"""    

antall = 0
for tegn in streng:
    antall += 1
print(f'Strengen var {antall} tegn lang')

# Alternativet er å skrive
print(f'Strengen var {len(streng)} tegn lang')

# len(streng) er en funksjon - og kalles det fordi den gjør noe for oss
# De fleste funksjoner bruker vi fordi de returnerer noe av verdi til oss,
# som len(). Andre bruker vi fordi de gjør noe for oss, som print()
# Vi ser ikke hvordan koden til len() egentlig er, ikke bryr vi oss heller!

# len(), print(), int(), float(), ord(), chr(), input()
# Alle disse har dere brukt, men de oppfører seg litt ulikt.
# alle returnerer noe, men hva returnerer print?

hva_returnerer_print = print('hva_returnerer_print')
print("hva_returnerer_print:",hva_returnerer_print)
print(f'Typen til hva_returnerer_print: {type(hva_returnerer_print)}')


# Hvordan skriver man en funksjon selv? La oss først tenke på en som
# gir et svar ut.

                  # Første linje: 'funksjonshode' - husk parenteser og kolon!
def funksjonsnavn(innparameter1, innparameter2): # 1 og 41 blir her parametre
    # kodeblokk med ting en må gjøre for å komme til svaret
    svar = innparameter1 + innparameter2 # Bare et eksempel, altså
    return svar # valgfritt om en returnerer noe spesifikt

# funksjonen kalles så med
beregning = funksjonsnavn(1,41) # 1 og 41 her kalles 'argumenter'
print(beregning)


# La meg lage en eksempelfunksjon som jeg kommer på underveis.




# Eksempel: Hvor mange vokaler er det i en streng
# Kan du lage en funksjon antall_vokaler som returnerer
# hvor mange vokaler det er i strengen?


# Hvorfor bruker vi funksjoner?
# - gruppere kode så en lettere ser hva som foregår
# - skriv kode en gang, gjenbruk det andre steder
# - flytter beregninger ut fra programmet som styrer flyt

# sjakkoppgave?
# foil 25 fra Terje.
# lag en løkke som ber brukeren skrive inn et gyldig sjakkfelt [a-h1-8]
# returner hvilken farge feltet har
# regel: for a, c, e, g: oddetall svarte, partal hvite, motsatt for resten

# han gjør noe veldig bra i koden sin i foilsettet, han flytter
# tallberegning ut av løkken. Det øker lesbarheten!
# Jeg tar bare med et alternativ her, som er litt mer kompakt,
# for å vise at dette kan gjøres på veldig mange måter.
'''
def sjakk():
    ordentlig = input('sjakkrute: ')

    while not (len(ordentlig) == 2 and ordentlig[0] in 'abcdefgh' and
               (0 < int(ordentlig[1]) < 9)):
        ordentlig = input('skriv inn noe riktigere, du: ')
    if ordentlig[0] in 'aceg':
        if ordentlig[1] in '02468': return 'Hvit'
        else: return 'Svart'
    else: # Legg merke til at jeg sjekker om tallene er TEGNENE for partall
        if ordentlig[1] in '02468': return 'Svart'
        else: return 'Hvit'
    
print(sjakk())

'''

# ppt side 38: top-down design

# Vise eksempel funksjon_kaller_funksjon_kaller_funksjon.py
# Legg merke til at print etter return i a og b aldri kalles.

# ppt side 42 viktig - lokale variable
'''
def hent_navn():
    navn = input('Skriv navnet: ') # Lokal variabel!
    # Variabelen navn kan kun brukes her!!!

def main():
    hent_navn()
    print('Hallo',navn) # gir feilmelding!!!
main()  
'''

# Et annet eksempel, som bruker samme navn på variable
# men det er altså ikke samme variabel

def foo(inn): 
    tall = inn*2 # tall her
    print(tall)  # er ikke den samme variabelen
    
tall = 3         # som tall her
print(tall)
foo(tall)

# Hvorfor er det sånn? ppt side 44 forteller om 'skop' (scope).
# En variabel lever i sin egen omgivelse. Når den deklareres inni
# en funksjon så lever den bare så lenge som funksjonen lever.
# Når funksjonen er ferdig slettes variabelen.
# Derfor kan man overføre verdier via return!

# se på koden i skop.py

# Oppgave:
# - lag en funksjon areal_sirkel som skal ha en parameter radius
# - den skal skrive ut radius og areal for sirkelen
# - Hint: import math og math.pi
"""