#coding=utf-8
# For - loopies
alfabet = 'abcdefghijklmnopqrstuvwxyzæøå'

# Den enkleste - fra 0 til og uten et heltall

# Den litt mer avanserte: fra et tall til og uten et annet

# Enda mer avansert: Fra et tall, til og uten et annet tall, men hopp n hver gang!

# Avansertest: Kan man loope tegnene i en streng?

# Avansertestestere: Kan man... uhm...
# Hvis jeg vil loope gjennom mange ulike tall jeg vet på forhånd...
# Er det noe i senere pensum som kan hjelpe meg med det? Pliiiis!!!1!
# Hint: la oss kikke litt på lister. De skrives med slike: []

# Avansertestest: kan vi... uhm... gå nedover? Telle baklengs? fra bakerste bokstav?

# Avansertestestere: Vi får se om vi har tid til en oppgave eller to om for-løkker.
# Som:
    # Skriv ut annenhver bokstav fra følgende variabel, bakfra: (først 'n', så 'g', så 'o' osv...
streng = 'detteernåenutruuuuliglangstrengsomi' \
         'allefallikkejeggidderåtelletegnenetilfordahadde' \
         'jegsikkertholdtpåtilimorgen!'

"""
    # Denne under tar du hjemme, den tar for mye tid nå.
    # spør hvor stort hopp du skal gjøre mellom hvert tegn
    # for hvert hopp gjennom strengen skriver du ut om bokstaven er en vokal:
    # bokstav in 'aeiouyæøå'

# Det siste: break og continue
# break kan presse seg ut av en løkke, HELT ut
nok = 0
while True:
    if nok > 1000:
        break
    nok += 1
print(nok)

# Oppgave: (Neppe på forelesning, men dere kan gjøre den selv)
# Kan dere lage en evig løkke, som spør om navn,
# og det gjør den helt til navnet gitt har to a'er?

# continue presser seg ut av denne 'iterasjonen' gjennom løkka, og starter øverst igjen

# Skriv ut tall delelige på alle av 3, 7 og 13:
# Hvis det ikke er delelig på 13, da kan vi hoppe rett ut med en gang
for i in range(1,1000):
    if i % 13:
        continue    
    if i % 7:
        continue    
    if i % 3:
        continue
    print(i)
"""